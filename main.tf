module "create_bucket_one" {
  #source       = "../module/"
  source                     = "git::https://gitlab.com/terraform_can/module.git"
  bucket_name                = "bucket1"
  object_lock_retention_days = 30
  deep_archive_days          = 120
  archive_days               = 90
}